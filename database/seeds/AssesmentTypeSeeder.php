<?php

use Illuminate\Database\Seeder;

class AssesmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	   $lov_group = 'assesment_type';

           DB::statement("DELETE FROM lovs WHERE lov_group='$lov_group'");
           $now = \Carbon\Carbon::now()->toDateTimeString();

           $assestment_type_list = [
           	  [  "lov_key" => 'VG', 'lov_value' => 'very_good', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],
           	  [  "lov_key" => 'G', 'lov_value' => 'good', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],
           	  [  "lov_key" => 'QG', 'lov_value' => 'quite_good', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],	
           	  [  "lov_key" => 'VB', 'lov_value' => 'very_bad', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],
           	  [  "lov_key" => 'B', 'lov_value' => 'bad', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],
           	  [  "lov_key" => 'QB', 'lov_value' => 'quite_bad', 'lov_group' => $lov_group, 'created_at' => $now, 'updated_at' => $now ],	
           ];

           DB::table('lovs')->insert($assestment_type_list);
    }
}
