<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeExperience extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_experiences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('employee_id');
			$table->string('work_place_name');
			$table->string('work_position');
			$table->integer('start_year')->nullable();
			$table->integer('end_year')->nullable();
			$table->timestamps();

			$table->foreign('employee_id')
			->references('id')->on('employees')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_experiences');
	}

}
