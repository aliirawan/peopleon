<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEmergencyContact extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_emergency_contact', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('employee_id');			
			$table->string('emergency_contact_person');
			$table->string('emergency_person_relation');
			$table->string('emergency_person_phone');
			$table->timestamps();
				
			$table->foreign('employee_id')
			->references('id')->on('employees')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_emergency_contact');
	}

}
