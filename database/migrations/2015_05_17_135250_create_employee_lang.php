<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLang extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_lang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('employee_id');
			$table->string('lang_name');
			$table->boolean('lang_active')->default(false);
			$table->boolean('lang_passive')->default(false);
			$table->timestamps();
			
			$table->foreign('employee_id')
			->references('id')->on('employees')
			->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_lang');
	}

}
