<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployeeAssesmentRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_assesment_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employee_id');

            $table->string('assesment_type'); // see to lov_group assesment_type
            $table->date("entry_date");    
            $table->text("description");
            
            $table->timestamps();

             $table->foreign('employee_id')
                ->references('id')->on('employees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_assesment_record');
    }
}
