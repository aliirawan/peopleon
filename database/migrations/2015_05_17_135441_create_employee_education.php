<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEducation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_education', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('employee_id');
			$table->string('school_name');
			$table->string('grade'); // look at lov SCHOOL_GRADE
			$table->integer('start_year')->nullable();
			$table->integer('end_year')->nullable();
			$table->timestamps();
			
			$table->foreign('employee_id')
			->references('id')->on('employees')
			->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_education');
	}

}
