<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSpecialDisease extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_special_disease', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('employee_id');
			$table->string('disease_name');
			$table->text('remark');
			$table->timestamps();
			
			$table->foreign('employee_id')
			->references('id')->on('employees')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_special_disease');
	}

}
