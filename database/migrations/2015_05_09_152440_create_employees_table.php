<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('code');
			$table->string('job_position');  // look at lov JOB_POSITION
			$table->string('full_name');
			$table->string('other_name')->nullable();
			$table->string('nick_name')->nullable();
			$table->string('gender', 1)->default('M'); // M - Male, F - Female
			$table->date('dob')->nullable();
			$table->string('current_address_line1')->nullable();
			$table->string('current_address_line2')->nullable();
			$table->string('permanent_address_line1')->nullable();
			$table->string('permanent_address_line2')->nullable();
			$table->string('phone1')->nullable();
			$table->string('phone2')->nullable();
			$table->string('mobile_phone1')->nullable();
			$table->string('mobile_phone2')->nullable();
			$table->string('bb_pin')->nullable();
			$table->string('line_no')->nullable();
			$table->string('email')->nullable();
			$table->bigInteger('main_photo')->nullable(); // media id
			$table->longtext('remark')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
