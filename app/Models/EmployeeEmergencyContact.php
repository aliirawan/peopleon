<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmergencyContact extends Model {

	//
	public $table = 'employee_emergency_contact';
	public $fillable = ["employee_id","emergency_contact_person","emergency_person_relation","emergency_person_phone"];
}
