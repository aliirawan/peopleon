<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeInterest extends Model {

	//
	public $table = 'employee_interest';
	public $fillable = ["employee_id","interest_name"];
}
