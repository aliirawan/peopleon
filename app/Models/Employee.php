<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {

	//
	public $fillable = ["code","job_position","full_name","other_name","nick_name","gender","dob",
	         "current_address_line1","current_address_line2",
	         "permanent_address_line1", "permanent_address_line2",
	         "phone1","phone2","mobile_phone1","mobile_phone2","bb_pin","line_no","email","remark","main_photo"];

	/**
     * Get the employee interests
     */
    public function interests()
    {
        return $this->hasMany('App\Models\EmployeeInterest');
    }     

    /**
     * Get the employee diseases
     */
    public function diseases()
    {
        return $this->hasMany('App\Models\EmployeeDisease');
    }         

    /**
     * Get the employee emergency contacts
     */
    public function emergency_contacts()
    {
        return $this->hasMany('App\Models\EmployeeEmergencyContact');
    }         
}
