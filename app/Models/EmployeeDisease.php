<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDisease extends Model {

	//
	public $table = 'employee_special_disease';
	public $fillable = ["employee_id","disease_name","remark"];
}
