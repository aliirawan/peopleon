<?php namespace App\Http\Middleware;

use Closure, Session, Auth, Log, App;

class MainMiddleware {

    /**
     * The availables languages.
     *
     * @array $languages
     */
    protected $languages = ['en','zh-TW'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug("Current lang detected: " . App::getLocale());

        if(!Session::has('locale'))
        {
             Session::put('locale', $request->getPreferredLanguage($this->languages));
        }

        App::setLocale(Session::get('locale'));

        // if(!Session::has('statut')) 
        // {
        //     Session::put('statut', Auth::check() ?  Auth::user()->role->slug : 'visitor');
        // }

        return $next($request);
    }

}