<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Flysystem\Dropbox\DropboxAdapter;
use League\Flysystem\Filesystem;
use Dropbox\Client;

class MediaController extends Controller {

	/**
	 * Upload single
	 * Accept:
	 *    - file
	 */
	public function uploadSingle(){
		\Log::debug("Media upload");
		
		if(\Request::hasFile('file')){

			if(\Request::file('file')->isValid()){

				$destinationPath = public_path('uploads');

				$file = \Request::file('file');
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$fileName = uniqid().'.'.$extension; // renameing image
				
				$file->move($destinationPath, $fileName); // uploading file to given path

				$fullPath = "uploads/$fileName";
				return response()->json([
						"status" => "OK",
						"files" => [
							$fullPath
						]
					]);
			}

		}

	}

}