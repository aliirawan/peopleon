<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeInterest;

use Illuminate\Http\Request;

class EmployeeInterestController extends Controller {

	public function index($employee_id){
		$interests = Employee::find($employee_id)->interests()->orderBy('interest_name')->get();
		return $interests;
	}

	public function store(){
		
		// TODO VALIDATION
		\Log::debug(\Input::all());

		$record = new EmployeeInterest(\Input::all());
		$record->save();

		return response()->json([
					"status" => "OK",
					"payload" => $record
				]);
	}

	public function destroy($employee_interest_id){
		
		$record = EmployeeInterest::findOrFail($employee_interest_id);
		$record->delete();

		return response()->json([
					"status" => "OK"
				]);
	}
}

?>