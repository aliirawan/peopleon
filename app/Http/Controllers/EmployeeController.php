<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Lov;

use Illuminate\Http\Request;

class EmployeeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		return Employee::join('lovs','lovs.lov_key','=','employees.job_position')
		     ->select('employees.*','lovs.lov_value as job_position_name')
		     ->orderBy('full_name')->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		\Log::debug(\Input::all());

		$rules = [
			"code" => "required",
			"full_name" => "required"
		];
		$validator = \Validator::make( \Input::all(), $rules );
    	if ($validator->fails())
		{
			return response()->json([
							"status" => "FAIL",
							"errors" => $validator->messages()
					]);	
		}

		$record = new Employee(\Input::all());
		$record->save();

		return response()->json([
					"status" => "OK"
				]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Employee::findOrFail($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = [
			"code" => "required",
			"full_name" => "required"
		];
		$validator = \Validator::make( \Input::all(), $rules );
    	if ($validator->fails())
		{
			return response()->json([
							"status" => "FAIL",
							"errors" => $validator->messages()
					]);	
		}

		$record = Employee::findOrFail($id);
		$record->fill (\Input::all());
		$record->save();

		return response()->json([
					"status" => "OK"
				]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Employee::findOrFail($id);
		$record->delete();

		return response()->json([
					"status" => "OK"
				]);	
	}

}
