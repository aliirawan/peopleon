<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Lov;

use Illuminate\Http\Request;

class JobPositionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Lov::where("lov_group","=","jobposition")->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = [
			"code" => "required",
			"name" => "required"
		];
		$validator = \Validator::make( \Input::all(), $rules );
    	if ($validator->fails())
		{
			return response()->json([
							"status" => "FAIL",
							"errors" => $validator->messages()
					]);	
		}

		$record = new Lov;
		$record->lov_key = \Input::get('code');
		$record->lov_value = \Input::get('name');
		$record->lov_group = 'jobposition';
		$record->save();

		return response()->json([
					"status" => "OK"
				]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Lov::findOrFail($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = [
			"code" => "required",
			"name" => "required"
		];
		$validator = \Validator::make( \Input::all(), $rules );
    	if ($validator->fails())
		{
			return response()->json([
							"status" => "FAIL",
							"errors" => $validator->messages()
					]);	
		}

		$record = Lov::findOrFail($id);
		$record->lov_key = \Input::get('code');
		$record->lov_value = \Input::get('name');
		$record->save();

		return response()->json([
					"status" => "OK"
				]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Lov::findOrFail($id);
		$record->delete();

		return response()->json([
					"status" => "OK"
				]);	
	}

}
