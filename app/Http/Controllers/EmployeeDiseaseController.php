<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeDisease;

use Illuminate\Http\Request;

class EmployeeDiseaseController extends Controller {

	public function index($employee_id){
		$diseases = Employee::find($employee_id)->diseases()->orderBy('disease_name')->get();
		return $diseases;
	}

	public function store(){
		
		// TODO VALIDATION
		\Log::debug(\Input::all());

		$record = new EmployeeDisease(\Input::all());
		$record->save();

		return response()->json([
					"status" => "OK",
					"payload" => $record
				]);
	}

	public function destroy($employee_disease_id){
		
		$record = EmployeeDisease::findOrFail($employee_disease_id);
		$record->delete();

		return response()->json([
					"status" => "OK"
				]);
	}
}

?>