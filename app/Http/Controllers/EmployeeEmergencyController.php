<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeEmergencyContact;

use Illuminate\Http\Request;

class EmployeeEmergencyController extends Controller {

	public function index($employee_id){
		$contacts = Employee::find($employee_id)->emergency_contacts()->orderBy('emergency_contact_person')->get();
		return $contacts;
	}

	public function store(){
		
		// TODO VALIDATION
		\Log::debug(\Input::all());

		$record = new EmployeeEmergencyContact(\Input::all());
		$record->save();

		return response()->json([
					"status" => "OK",
					"payload" => $record
				]);
	}

	public function destroy($contact_id){
		
		$record = EmployeeEmergencyContact::findOrFail($contact_id);
		$record->delete();

		return response()->json([
					"status" => "OK"
				]);
	}
}

?>