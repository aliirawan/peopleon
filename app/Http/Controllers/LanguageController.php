<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Session;

class LanguageController extends Controller {

	public function set($locale){
		Session::put('locale', $locale);
	}
}