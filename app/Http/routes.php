<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){ return redirect('home'); });

//Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/home', 'HomeController@index');
Route::get('/jobposition', function(){ return view('jobposition'); });
Route::get('/employee', function(){ return view('employee'); });
Route::get('/lang/{locale}', 'LanguageController@set');

/**
 * Media Controlle for upload/download media
 */
Route::post('/media/upload/single', 'MediaController@uploadSingle');
Route::post('/media/upload/multiple', 'MediaController@uploadMultiple');

Route::group(['prefix' => 'api'], function()
{
    Route::resource('jobposition', 'JobPositionController');
    Route::resource('employee', 'EmployeeController');

    Route::get('employee/interests/{id}', 'EmployeeInterestController@index');
    Route::post('employee/interests', 'EmployeeInterestController@store');
    Route::delete('employee/interests/{id}', 'EmployeeInterestController@destroy');


    Route::get('employee/diseases/{id}', 'EmployeeDiseaseController@index');
    Route::post('employee/diseases', 'EmployeeDiseaseController@store');
    Route::delete('employee/diseases/{id}', 'EmployeeDiseaseController@destroy');

    Route::get('employee/emergency/{id}', 'EmployeeEmergencyController@index');
    Route::post('employee/emergency', 'EmployeeEmergencyController@store');
    Route::delete('employee/emergency/{id}', 'EmployeeEmergencyController@destroy');
});