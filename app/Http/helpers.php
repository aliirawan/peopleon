<?php

function get_current_lang(){
	switch(App::getLocale()){
		case 'en': return trans('label.english');
		case 'zh-TW': return trans('label.chinese');
	}
}