<?php
    return [

    	// C
    	"chinese" => "Chinese",

    	// D
    	"dashboard" => "Dashboard",
        "data" => "Data",

    	// E
    	"employee" => "Employee",
    	"english" => "English",

    	// G
    	"general_data" => "General Data",

    	// J
		"job_position" => "Job Position",    	

        // Y
        "you_are_here" => "You are here",
        
    	"good" => "Good",
    	"very_good" => "Very Good",
    	"quite_good" => "Quite Good",

		"bad" => "Bad",
    	"very_bad" => "Very Bad",
    	"quite_bad" => "Quite Bad",
    ];