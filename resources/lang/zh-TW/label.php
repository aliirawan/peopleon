<?php
    return [

        // C
        "chinese" => "中文",

        // D
        "dashboard" => "儀表盤",
        "data" => "資料",

    	// E
    	"employee" => "僱員",
        "english" => "英文",

        // G
        "general_data" => "一般資料",

        // J
        "job_position" => "工作職位",       

        // Y
        "you_are_here" => "你在这里",

    	"good" => "Good",
    	"very_good" => "Very Good",
    	"quite_good" => "Quite Good",

		"bad" => "Bad",
    	"very_bad" => "Very Bad",
    	"quite_bad" => "Quite Bad",
    ];