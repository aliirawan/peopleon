@extends('master')
@section('title')
	Dashboard
@endsection
@section('content')

    <!-- main page content. the place to put widgets in. usually consists of .row > .col-md-* > .widget.  -->
    <main id="content" class="content" role="main" ng-app="starter">
    	<div ui-view />
    </main>

@endsection
@section('before-end-body')
	<script src="/app/main.js"></script>
@endsection