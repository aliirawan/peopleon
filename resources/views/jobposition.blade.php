@extends('master')
@section('title')
	Job Position
@endsection
@section('content')

    <!-- main page content. the place to put widgets in. usually consists of .row > .col-md-* > .widget.  -->
    <main id="content" class="content" role="main" ng-app="starter">
    	<div ui-view />
    </main>

@endsection
@section('before-end-body')
	<script src="/app/service/jobposition_service.js"></script>
	<script src="/app/jobposition.js"></script>
	<script>
	$(document).ready(function(){
		pageSet('page_jobposition');
	});
	</script>
@endsection