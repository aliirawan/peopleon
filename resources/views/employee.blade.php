@extends('master')
@section('title')
	Employee
@endsection
@section('content')

    <!-- main page content. the place to put widgets in. usually consists of .row > .col-md-* > .widget.  -->
    <main id="content" class="content" role="main" ng-app="starter">
    	<div ui-view />
    </main>

@endsection
@section('before-end-body')
	<script src="vendor/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
	<script src="vendor/jquery-autosize/jquery.autosize.min.js"></script>
	<script src="vendor/moment/min/moment.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script src="/app/modules/media.js"></script>
	<script src="/app/service/jobposition_service.js"></script>
	<script src="/app/service/employee_service.js"></script>
	<script src="/app/employee.js"></script>
	<script>
	$(document).ready(function(){
		pageSet('page_employee');
	});
	</script>
@endsection