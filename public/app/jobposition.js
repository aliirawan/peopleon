/**
 * Module handler for page /jobposition
 *
 * @author Ali Irawan (boylevantz@gmail.com)
 */
var app = angular.module('starter',['ui.router','jobposition-service', 'ngBack','gettext']);
app.run(function(gettextCatalog){
    gettextCatalog.setCurrentLanguage($('#_curr_lang').val());
});

app.config(function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise("index");

	$stateProvider
    	.state('index', {
      			url: "/index",
      			templateUrl: "templates/jobposition/list.html",
            controller: "JobPositionCtrl"
    	})
      .state('add', {
            url: "/add",
            templateUrl: "templates/jobposition/add.html",
            controller: "AddJobPositionCtrl"
      })
      .state('edit', {
            url: "/edit/{id}",
            templateUrl: "templates/jobposition/edit.html",
            controller: "EditJobPositionCtrl"
      })
});
app.controller('JobPositionCtrl',['$scope','JobPositionService', function($scope, JobPositionService){

    $scope.loadData = function(){
      JobPositionService.list()
      .success(function(data){
          $scope.list = data;
      })
      .error(function(error){
          $scope.list = null;  
      });
    }

    $scope.remove = function(item){

        swal({   
           title: "Are you sure?",   
           text: " '" + item.lov_value +"' will be deleted!",   
           type: "warning",   
           showCancelButton: true,   
           confirmButtonColor: "#DD6B55",   
           confirmButtonText: "Yes, delete it!",   
           closeOnConfirm: false }, function(){   

            JobPositionService.remove(item.id)
              .success(function(data){
                 swal("Remove success!", "Your data has been removed.", "success");
                 $scope.loadData();
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });

         });

    }

    $scope.loadData();
}]);
app.controller('AddJobPositionCtrl',['$scope','$state','JobPositionService', function($scope, $state, JobPositionService){
    $scope.input = {};

    $scope.save = function(isValid){
        if(isValid){
            JobPositionService.add($scope.input)
            .success(function(data){
                 if(data.status == 'OK'){
                      swal("Save success!", "Your data has been saved.", "success");
                      $state.go('index');
                 } else {
                      swal("Something goes wrong!", "Unable to save.", "error");
                 }
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });
        }
    }

}]);
app.controller('EditJobPositionCtrl',['$scope','$state','$stateParams','JobPositionService', function($scope, $state, $stateParams, JobPositionService){

    $scope.input = {};

    JobPositionService.show($stateParams.id)
    .success(function(data){
        $scope.input = {
              id: data.id,
              code: data.lov_key,
              name: data.lov_value
        }
    })
    .error(function(error){
        swal("Something goes wrong!", "Please try again or contact administrator.", "error");
    })

    $scope.save = function(isValid){
        if(isValid){
            JobPositionService.edit($scope.input.id, $scope.input)
            .success(function(data){
                 if(data.status == 'OK'){
                      swal("Save success!", "Your data has been saved.", "success");
                      $state.go('index');
                 } else {
                      swal("Something goes wrong!", "Unable to save.", "error");
                 }
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });
        }
    }

}]);