angular.module('jobposition-service',[])
.service('JobPositionService', ['$http', function($http){

	return {
		list: function(){
			return $http.get('/api/jobposition');
		},
		add: function(input){
			return $http.post('/api/jobposition', input);	
		},
		show: function(id){
			return $http.get('/api/jobposition/' + id);	
		},
		edit: function(id, input){
			return $http.put('/api/jobposition/' + id, input);	
		},
		remove: function(id){
			return $http.delete('/api/jobposition/' + id);	
		},
	}
}]);