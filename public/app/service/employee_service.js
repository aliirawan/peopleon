angular.module('employee-service',[])
.service('EmployeeService', ['$http', function($http){

	return {
		list: function(){
			return $http.get('/api/employee');
		},
		add: function(input){
			return $http.post('/api/employee', input);	
		},
		show: function(id){
			return $http.get('/api/employee/' + id);	
		},
		edit: function(id, input){
			return $http.put('/api/employee/' + id, input);	
		},
		remove: function(id){
			return $http.delete('/api/employee/' + id);	
		},

		// More info Interests
		getInterests: function(id){
			return $http.get('/api/employee/interests/' + id);		
		},
		addInterest: function(object){
			return $http.post('/api/employee/interests', object);		
		},
		removeInterest: function(id){
			return $http.delete('/api/employee/interests/' + id);		
		},

		// More info Diseases
		getDiseases: function(id){
			return $http.get('/api/employee/diseases/' + id);		
		},
		addDisease: function(object){
			return $http.post('/api/employee/diseases', object);		
		},
		removeDisease: function(id){
			return $http.delete('/api/employee/diseases/' + id);		
		},

		// More info Emergency Contact
		getEmergencyContacts: function(id){
			return $http.get('/api/employee/emergency/' + id);		
		},
		addEmergencyContact: function(object){
			return $http.post('/api/employee/emergency', object);		
		},
		removeEmergencyContact: function(id){
			return $http.delete('/api/employee/emergency/' + id);		
		}
	}
}]);