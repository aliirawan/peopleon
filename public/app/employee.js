/**
 * Module handler for page /employee
 *
 * @author Ali Irawan (boylevantz@gmail.com)
 */
var app = angular.module('starter',['ui.router','app-media', 'employee-service','jobposition-service', 'ngBack','gettext']);
app.run(function(gettextCatalog){
    gettextCatalog.setCurrentLanguage($('#_curr_lang').val());
});

app.config(function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise("index");

	$stateProvider
    	.state('index', {
      			url: "/index",
      			templateUrl: "templates/employee/list.html",
            controller: "EmployeeCtrl"
    	})
      .state('add', {
            url: "/add",
            templateUrl: "templates/employee/add.html",
            controller: "AddEmployeeCtrl"
      })
      .state('edit', {
            url: "/edit/{id}",
            templateUrl: "templates/employee/edit.html",
            controller: "EditEmployeeCtrl"
      })
      .state('more', {
            url: "/more/{id}",
            templateUrl: "templates/employee/more-jobexp.html",
            controller: "MoreEmployeeInfoJobExpCtrl"
      })
      .state('more/education', {
            url: "/more/{id}/education",
            templateUrl: "templates/employee/more-education.html",
            controller: "MoreEmployeeInfoEducationCtrl"
      })
      .state('more/skills', {
            url: "/more/{id}/skills",
            templateUrl: "templates/employee/more-skills.html",
            controller: "MoreEmployeeInfoSkillsCtrl"
      })
      .state('more/lang', {
            url: "/more/{id}/lang",
            templateUrl: "templates/employee/more-lang.html",
            controller: "MoreEmployeeInfoLangCtrl"
      })
      .state('more/interest', {
            url: "/more/{id}/interest",
            templateUrl: "templates/employee/more-interest.html",
            controller: "MoreEmployeeInfoInterestCtrl"
      })
      .state('more/disease', {
            url: "/more/{id}/disease",
            templateUrl: "templates/employee/more-disease.html",
            controller: "MoreEmployeeInfoDiseaseCtrl"
      })
      .state('more/emergency', {
            url: "/more/{id}/emergency",
            templateUrl: "templates/employee/more-emergency.html",
            controller: "MoreEmployeeInfoEmergencyCtrl"
      })
});
app.controller('EmployeeCtrl',['$scope','EmployeeService', function($scope, EmployeeService){

    $scope.loadData = function(){
      EmployeeService.list()
      .success(function(data){
          $scope.list = data;
      })
      .error(function(error){
          $scope.list = null;  
      });
    }

    $scope.remove = function(item){

        swal({   
           title: "Are you sure?",   
           text: " Employee " + item.full_name + " will be deleted!",   
           type: "warning",   
           showCancelButton: true,   
           confirmButtonColor: "#DD6B55",   
           confirmButtonText: "Yes, delete it!",   
           closeOnConfirm: false }, function(){   

            EmployeeService.remove(item.id)
              .success(function(data){
                 swal("Remove success!", "Your data has been removed.", "success");
                 $scope.loadData();
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });

         });

    }

    $scope.loadData();
}]);
app.controller('AddEmployeeCtrl',['$scope','$state','EmployeeService', 'JobPositionService','MediaUploader', 
         function($scope, $state, EmployeeService, JobPositionService, MediaUploader){

    $scope.input = {
        gender: 'M'
    };
    $('#dob').datetimepicker({
            format: 'MM/DD/YYYY'
    });
    $(".autogrow").autosize({append: "\n"});
    $('.fileinput').fileinput({
        name: 'file'
    });

    $('#file').on('change.bs.fileinput', function () {
       
       MediaUploader.upload('file', 
        function(response){
            // alert(response);
            if(response.status=='OK'){
                 $('#main_photo').val(response.files[0]);
            }
        });

    });

    // Load job position
    JobPositionService.list()
    .success(function(data){
        $scope.job_position_list = data;
    })
    .error(function(error){
        swal("Something goes wrong!", "Please try again or contact administrator.", "error");
    });

    $scope.save = function(isValid){
        if(isValid){
            EmployeeService.add($scope.input)
            .success(function(data){
                 console.log(data);
                 if(data.status == 'OK'){
                      swal("Save success!", "Your data has been saved.", "success");
                      $state.go('index');
                 } else {
                      swal("Something goes wrong!", "Unable to save.", "error");
                 }
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });
        }
    }

}]);
app.controller('EditEmployeeCtrl',['$scope','$state','$stateParams','EmployeeService','JobPositionService', 
      function($scope, $state, $stateParams, EmployeeService, JobPositionService){

    $scope.input = {};

    // Load job position
    JobPositionService.list()
    .success(function(data){
        $scope.job_position_list = data;
    })
    .error(function(error){
        swal("Something goes wrong!", "Please try again or contact administrator.", "error");
    });

    EmployeeService.show($stateParams.id)
    .success(function(data){
        $scope.input = data;
    })
    .error(function(error){
        swal("Something goes wrong!", "Please try again or contact administrator.", "error");
    })

    $scope.save = function(isValid){
        if(isValid){
            EmployeeService.edit($scope.input.id, $scope.input)
            .success(function(data){
                 if(data.status == 'OK'){
                      swal("Save success!", "Your data has been saved.", "success");
                      $state.go('index');
                 } else {
                      swal("Something goes wrong!", "Unable to save.", "error");
                 }
            })
            .error(function(error){
                 swal("Something goes wrong!", "Please try again or contact administrator.", "error");
            });
        }
    }

}]);
app.controller('MoreEmployeeInfoJobExpCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in jobexp
      $timeout(function(){
        $('ul#employee_info_nav li#jobexp').addClass('active');   
      }, 100);
      
      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#jobexp').addClass('active'); 
      });

      // TODO fill in code for add/edit/remove job experiences data

}]);
app.controller('MoreEmployeeInfoEducationCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in education
      $timeout(function(){
        $('ul#employee_info_nav li#education').addClass('active'); 
      }, 100);

      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#education').addClass('active'); 
      });

      // TODO fill in code for add/edit/remove education history

}]);
app.controller('MoreEmployeeInfoSkillsCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in skills
      $timeout(function(){
        $('ul#employee_info_nav li#skills').addClass('active'); 
      }, 100);

      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#skills').addClass('active'); 
      });

      // TODO fill in code for add/edit/remove skills

}]);
app.controller('MoreEmployeeInfoLangCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in lang
      $timeout(function(){
        $('ul#employee_info_nav li#lang').addClass('active'); 
      }, 100);
          
      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#lang').addClass('active'); 
      });

      // TODO fill in code for add/edit/remove languages

}]);
app.controller('MoreEmployeeInfoInterestCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in interests
      $timeout(function(){
        $('ul#employee_info_nav li#interest').addClass('active'); 
      }, 100);
        
      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#interest').addClass('active'); 
      });

      $scope.load = function(){
        EmployeeService.getInterests($scope.input.employee_id)
         .success(function(data){
              // console.log(data);
              $scope.list = data;
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }
      $scope.add = function(){
        EmployeeService.addInterest($scope.input)
         .success(function(data){
              if(data.status=='OK'){
                $scope.list.push(data.payload);
                $scope.list.sort(function(a, b) {
                    if (a.interest_name < b.interest_name)
                      return -1;
                    if (a.interest_name > b.interest_name)
                      return 1;
                    return 0;
                });
                $scope.input.interest_name = '';

                $scope.form_input.$setPristine();
                $scope.form_input.$setUntouched();
                
              } else {
                swal("Something goes wrong!", "Unable to save", "error");  
              }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }            
      $scope.remove = function(obj){
        // console.log(obj.id);
        EmployeeService.removeInterest(obj.id)
         .success(function(data){
              for (var i=0; i < $scope.list.length; i++)
                 if ($scope.list[i].id === obj.id) {
                    $scope.list.splice(i,1);
                    break;
                 }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }



      $scope.load();
}]);
app.controller('MoreEmployeeInfoDiseaseCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in diseases
      $timeout(function(){
        $('ul#employee_info_nav li#disease').addClass('active'); 
      }, 100);
        
      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#disease').addClass('active'); 
      });

      $scope.load = function(){
        EmployeeService.getDiseases($scope.input.employee_id)
         .success(function(data){
              // console.log(data);
              $scope.list = data;
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }
      $scope.add = function(){
        EmployeeService.addDisease($scope.input)
         .success(function(data){
              if(data.status=='OK'){
                $scope.list.push(data.payload);
                $scope.input.disease_name = '';
                $scope.input.remark = ''; 

                $scope.form_input.$setPristine();
                $scope.form_input.$setUntouched();
                
                $('#disease_name').focus();
              } else {
                swal("Something goes wrong!", "Unable to save", "error");  
              }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }            
      $scope.remove = function(obj){
        // console.log(obj.id);
        EmployeeService.removeDisease(obj.id)
         .success(function(data){
              for (var i=0; i < $scope.list.length; i++)
                 if ($scope.list[i].id === obj.id) {
                    $scope.list.splice(i,1);
                    break;
                 }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }



      $scope.load();
            
}]);
app.controller('MoreEmployeeInfoEmergencyCtrl',['$scope','$state','$stateParams','EmployeeService','$timeout',
      function($scope, $state, $stateParams, EmployeeService, $timeout){

      $scope.input = {
        employee_id: $stateParams.id
      };

      // Set the active menu in emergency contacts
      $timeout(function(){
        $('ul#employee_info_nav li#emergency').addClass('active'); 
      }, 100);
        
      angular.element(document).ready(function () {
          $('ul#employee_info_nav li#emergency').addClass('active'); 
      });

      $scope.load = function(){
        EmployeeService.getEmergencyContacts($scope.input.employee_id)
         .success(function(data){
              // console.log(data);
              $scope.list = data;
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }
      $scope.add = function(){
        EmployeeService.addEmergencyContact($scope.input)
         .success(function(data){
              if(data.status=='OK'){
                $scope.list.push(data.payload);
                $scope.input.emergency_contact_person = '';
                $scope.input.emergency_person_relation = ''; 
                $scope.input.emergency_person_phone = ''; 
                
                $scope.form_input.$setPristine();
                $scope.form_input.$setUntouched();

                $('#emergency_contact_person').focus();
              } else {
                swal("Something goes wrong!", "Unable to save", "error");  
              }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }            
      $scope.remove = function(obj){
        // console.log(obj.id);
        EmployeeService.removeEmergencyContact(obj.id)
         .success(function(data){
              for (var i=0; i < $scope.list.length; i++)
                 if ($scope.list[i].id === obj.id) {
                    $scope.list.splice(i,1);
                    break;
                 }
         })
         .error(function(error){
              swal("Something goes wrong!", "Please try again or contact administrator.", "error");
        });
      }



      $scope.load();
            
}]);