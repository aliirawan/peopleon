var mod_media = angular.module('app-media',[]);
mod_media.service('MediaUploader',[function(){

   return {
   	   upload: function(fileId, successCallback){
   	    	       var fileInput = document.getElementById(fileId);
       			   var file = fileInput.files[0];

			       var formData = new FormData();
			       formData.append("file", file);
			       formData.append("_token", $('input[name="_token"]').val());

			       $.ajax({
			          url: '/media/upload/single',
			          data: formData,
			          cache: false,
			          contentType: false,
			          processData: false,
			          type: 'POST',
			          success: successCallback
			      });
   	   }
   }

}]);
