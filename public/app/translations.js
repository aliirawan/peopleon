angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('zh-TW', {"Add New Job":"加新","Code":"代碼","Dashboard":"儀表盤","Job Position":"工作職位","YOU ARE HERE":"你在這裡"});
    gettextCatalog.setStrings('en', {"Add New Job":"Add New Job","Code":"Code","Dashboard":"Dashboard","Job Position":"Job Position","YOU ARE HERE":"YOU ARE HERE"});
/* jshint +W100 */
}]);