var app = angular.module('starter',['ui.router']);
app.config(function($stateProvider, $urlRouterProvider) {
	
	//$urlRouterProvider.otherwise("/home");

	$stateProvider
    	.state('home', {
      			url: "/home",
      			templateUrl: "templates/home.html"
    	})
    	.state('jobposition', {
      			url: "/jobposition",
      			templateUrl: "templates/jobposition/list.html"
    	})
});
