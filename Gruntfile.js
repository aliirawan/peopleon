module.exports = function(grunt) {
        grunt.initConfig({
        	  nggettext_extract: {
			    pot: {
			      files: {
			        'po/template.pot': [
			                                'public/templates/*.html',
			                                'public/templates/jobposition/*.html',
			                           ]
			      }
			    },
			  },
			  nggettext_compile: {
			    all: {
			      files: {
			        'public/app/translations.js': ['po/*.po']
			      }
			    },
			  },
        });

        grunt.loadNpmTasks('grunt-angular-gettext');

        grunt.registerTask('default', ['nggettext_extract']);	
        grunt.registerTask('translate', ['nggettext_compile']);	
};